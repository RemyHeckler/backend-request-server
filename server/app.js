/* eslint-disable import/first */
import './load-config';

import express from 'express';
import bodyParser from 'body-parser';
import bluebird from 'bluebird';
import mongoose from 'mongoose';
import expressValidator from 'express-validator';
import './passport';

import './models/user';
import './models/request';

import auth from './routes/auth';
import user from './routes/users';
import requests from './routes/requests';

const app = express();
const port = process.env.PORT || 8080;

app.use(bodyParser.json());
app.use(expressValidator());

app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization');
  res.header('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, OPTIONS');
  next();
});

const startServer = () => {
  app.listen(port, () => console.log(`App started on port ${port}`));
};

const connectDb = () => {
  mongoose.Promise = bluebird;
  mongoose.connect(process.env.MONGOURL);
  return mongoose.connection;
};

app.use('/', auth);
app.use('/user', user);
app.use('/requests', requests);

app.use((err, req, res) => {
  res.status(500);
  res.render('error', 'Something going wrong');
});


connectDb()
  .on('error', console.log)
  // .on('disconnected', connectDb)
  .once('open', startServer);
