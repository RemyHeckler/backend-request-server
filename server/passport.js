import passport from 'passport';
import mongoose from 'mongoose';
import bcrypt from 'bcryptjs';
import passportJWT from 'passport-jwt';

import './models/user';

const LocalStrategy = require('passport-local').Strategy;

const JWTStrategy = passportJWT.Strategy;
const ExtractJWT = passportJWT.ExtractJwt;

const Users = mongoose.model('Users');

passport.use(new LocalStrategy(
  {
    usernameField: 'email',
    passwordField: 'password',
  },
  ((email, password, cb) => Users.findOne({ email })
    .then((user) => {
      if (!user || !(bcrypt.compareSync(password, user.password))) {
        return cb(null, false, { message: 'Incorrect email or password.' });
      }

      return cb(null, user, { message: 'Logged In Successfully' });
    })
    .catch(err => cb(err))),
));

passport.use(new JWTStrategy(
  {
    jwtFromRequest: ExtractJWT.fromAuthHeaderAsBearerToken(),
    secretOrKey: process.env.SECRET,
  },
  ((jwtPayload, cb) => Users.findOne(jwtPayload.id)
    .then(user => cb(null, user))
    .catch(err => cb(err))),
));
