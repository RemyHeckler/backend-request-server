import jwt from 'jsonwebtoken';
import passport from 'passport';

export default (req, res) => {
  passport.authenticate('local', { session: false }, (err, user) => {
    if (err) {
      return res.status(500).json({ message: 'Something failed' });
    }

    if (!user) {
      return res.status(401).json({ message: 'Wrong username or password' });
    }

    req.login(user, { session: false }, (error) => {
      if (error) {
        return res.send(error);
      }
      const token = jwt.sign(user.toJSON(), process.env.SECRET);
      return res.json({
        name: user.name,
        email: user.email,
        isAdmin: user.isAdmin,
        token,
      });
    });
  })(req, res);
};
