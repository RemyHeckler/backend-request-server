import mongoose from 'mongoose';
import bcrypt from 'bcryptjs';

import '../../models/user';

const Users = mongoose.model('Users');

export default async (req, res) => {
  try {
    const { name, email, password } = req.body;
    req.checkBody('name', 'Name is required').notEmpty();
    req.checkBody('email', 'Email is required').notEmpty();
    req.checkBody('email', 'Please enter a valid email').isEmail();
    req.checkBody('password', 'Password is required').notEmpty();
    req.checkBody('confirmPassword', 'Confirm Password is required').notEmpty();
    req.checkBody('confirmPassword', 'Confirm Password Must Matches With Password').equals(password);

    const errors = req.validationErrors();

    if (errors) {
      res.status(400);
      return res.send({ errors });
    }

    const savedUser = await Users.findOne({ email });

    if (savedUser) {
      res.status(409);
      return res.send({ error: 'user already exists' });
    }

    const salt = bcrypt.genSaltSync(10);
    const user = new Users({
      name,
      email,
      password: bcrypt.hashSync(password, salt),
      isAdmin: false,
    });

    await user.save();

    return res.send({ name, email });
  } catch (error) {
    res.status(500);
    return res.send({ error: 'Something failed!' });
  }
};
