import mongoose from 'mongoose';
import jwtDecode from 'jwt-decode';

import '../../models/request';

const Request = mongoose.model('Request');

export default async (req, res) => {
  try {
    const token = req.headers.authorization.replace('Bearer ', '');
    const activeUser = jwtDecode(token);
    const { _id, isAdmin } = activeUser;
    const { id } = req.query;

    if (!id) {
      res.status(400);
      return res.send('Id is required');
    }

    const request = await Request.findOne({ _id: id });

    if (!isAdmin && (!request.author || !request.author.equals(_id))) {
      res.status(403);
      return res.send('Access denied');
    }

    if (!request) {
      res.status(404);
      return res.send('Not found');
    }

    await Request.findByIdAndRemove(id);
    return res.send(`Request ${id} deleted`);
  } catch (e) {
    res.status(500);
    res.send('Something going wrong');
  }
};
