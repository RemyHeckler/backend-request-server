import mongoose from 'mongoose';
import jwtDecode from 'jwt-decode';

const Request = mongoose.model('Request');

export default async (req, res) => {
  try {
    const token = req.headers.authorization.replace('Bearer ', '');
    const activeUser = jwtDecode(token);
    const { _id, isAdmin } = activeUser;
    const { title, body, completed } = req.body;
    const id = req.query.id || req.body.id || _id;

    const request = await Request.findOne({ _id: id });

    if (!isAdmin && !request.author.equals(_id)) {
      res.status(401);
      return res.send('Access denied');
    }

    if (!request) {
      res.status(404);
      return res.send('Not found');
    }

    const values = JSON.parse(JSON.stringify({ title, body, completed }));

    const updatedRequest = await Request
      .findOneAndUpdate({ _id: id }, { ...values }, { new: true })
      .populate('author', '-password')
      .exec();
    return res.send(updatedRequest);
  } catch (e) {
    res.status(500);
    res.send('Something going wrong');
  }
};
