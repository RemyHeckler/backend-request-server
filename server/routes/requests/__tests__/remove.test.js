import mockingoose from 'mockingoose';

import '../../../models/user';
import '../../../models/request';
import remove from '../remove';


const fakeAdminToken = 'Bearer eyJhbGciOiJIUzI1NiJ9.eyJpc0FkbWluIjp0cnVlLCJfaWQiOiIxIn0.0hlJx77CWgtltAVkiLHSd6Gq5zjFI85PEz1NIFo9cqw';
const fakeNonOwnerAdminToken = 'Bearer eyJhbGciOiJIUzI1NiJ9.eyJpc0FkbWluIjpmYWxzZSwiX2lkIjoiMTIzMTIzMTIzMTIzIn0.hFR57WnI9t5jvZQM6NepA3LCS8fvhoM8tiIhIEkKe1U';

mockingoose.Request.toReturn({
  _id: '2',
  title: 'title',
  body: 'body',
  author: '2',
}, 'findOne');

mockingoose.Users.toReturn({
  _id: '2',
  name: 'name',
  email: 'name@email.com',
});


describe('remove Requests should works as designed', () => {
  it('Admin can remove requests', async () => {
    const removalId = '2';
    const req = {
      headers: {
        authorization: fakeAdminToken,
      },
      query: {
        id: removalId,
      },
    };

    const res = {
      status: jest.fn(),
      send: jest.fn(),
    };

    await remove(req, res);

    expect(res.send.mock.calls)
      .toHaveLength(1);
    expect(res.send.mock.calls[0][0])
      .toBe(`Request ${removalId} deleted`);
  });
  it('Non Admin or non-owner can\'t remove requests and should get 403', async () => {
    const removalId = '2';
    const req = {
      headers: {
        authorization: fakeNonOwnerAdminToken,
      },
      query: {
        id: removalId,
      },
    };

    const res = {
      status: jest.fn(),
      send: jest.fn(),
    };

    await remove(req, res);

    expect(res.send.mock.calls)
      .toHaveLength(1);
    expect(res.send.mock.calls[0][0])
      .toBe('Access denied');
    expect(res.status.mock.calls)
      .toHaveLength(1);
    expect(res.status.mock.calls[0][0])
      .toBe(403);
  });
  it('Id is required, server should response with 400 if id is not passed', async () => {
    const req = {
      headers: {
        authorization: fakeAdminToken,
      },
      query: {},
    };

    const res = {
      status: jest.fn(),
      send: jest.fn(),
    };

    await remove(req, res);

    expect(res.send.mock.calls)
      .toHaveLength(1);
    expect(res.send.mock.calls[0][0])
      .toBe('Id is required');
    expect(res.status.mock.calls)
      .toHaveLength(1);
    expect(res.status.mock.calls[0][0])
      .toBe(400);
  });
  it('Server should response with 404 if request is not exist', async () => {
    mockingoose.resetAll();

    const req = {
      headers: {
        authorization: fakeAdminToken,
      },
      query: {
        id: 'not found id',
      },
    };

    const res = {
      status: jest.fn(),
      send: jest.fn(),
    };

    await remove(req, res);

    expect(res.send.mock.calls)
      .toHaveLength(1);
    expect(res.send.mock.calls[0][0])
      .toBe('Not found');
    expect(res.status.mock.calls)
      .toHaveLength(1);
    expect(res.status.mock.calls[0][0])
      .toBe(404);
  });
});
