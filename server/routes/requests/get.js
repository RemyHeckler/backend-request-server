import mongoose from 'mongoose';
import jwtDecode from 'jwt-decode';

const Request = mongoose.model('Request');

export default async (req, res) => {
  try {
    const token = req.headers.authorization.replace('Bearer ', '');
    const activeUser = jwtDecode(token);
    const _id = req.query.id ? req.query.id : activeUser;

    if (!_id) {
      res.status(400);
      return res.send('Id is required');
    }

    const requests = await Request.find({ author: _id })
      .populate('author', '-password');
    return res.send(requests);
  } catch (e) {
    res.status(500);
    res.send('Something going wrong');
  }
};
