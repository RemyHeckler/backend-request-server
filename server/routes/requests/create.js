import mongoose from 'mongoose';
import jwtDecode from 'jwt-decode';

const Request = mongoose.model('Request');

export default async (req, res) => {
  try {
    const token = req.headers.authorization.replace('Bearer ', '');
    const activeUser = jwtDecode(token);
    const { _id } = activeUser;

    req.checkBody('title', 'Title is required').notEmpty();
    req.checkBody('body', 'Question is required').notEmpty();

    const errors = req.validationErrors();

    if (errors) {
      res.status(400);
      return res.send({ errors });
    }

    const request = new Request({
      author: _id,
      title: req.body.title,
      body: req.body.body,
      date: new Date().toString(),
      completed: false,
    });

    const savedRequest = await request.save();
    return res.send(await savedRequest.populate('author', '-password').execPopulate());
  } catch (e) {
    res.status(500);
    res.send('Something going wrong');
  }
};
