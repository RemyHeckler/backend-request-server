import express from 'express';
import passport from 'passport';

import remove from './remove';
import get from './get';
import update from './update';
import create from './create';
import requestsList from './requests-list';

const router = express.Router();

router.get('/list', passport.authenticate('jwt', { session: false }), requestsList);
router.get('/', passport.authenticate('jwt', { session: false }), get);
router.post('/', passport.authenticate('jwt', { session: false }), create);
router.put('/', passport.authenticate('jwt', { session: false }), update);
router.delete('/', passport.authenticate('jwt', { session: false }), remove);

export default router;
