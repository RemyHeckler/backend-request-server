import mongoose from 'mongoose';
import jwtDecode from 'jwt-decode';

const Request = mongoose.model('Request');

export default async (req, res) => {
  try {
    const token = req.headers.authorization.replace('Bearer ', '');
    const activeUser = jwtDecode(token);
    const { isAdmin } = activeUser;
    const { author } = req.query;
    const findParams = author ? { author } : {};

    if (!isAdmin) {
      res.status(403);
      res.send('Access denied');
    }

    const requests = await Request.find(findParams).populate('author', '-password');
    return res.send(requests);
  } catch (e) {
    res.status(500);
    res.send('Something going wrong');
  }
};
