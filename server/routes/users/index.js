import express from 'express';
import passport from 'passport';

import remove from './remove';
import getUser from './get';
import update from './update';
import usersList from './users-list';

const router = express.Router();

router.get('/list', passport.authenticate('jwt', { session: false }), usersList);
router.get('/', passport.authenticate('jwt', { session: false }), getUser);
router.put('/', passport.authenticate('jwt', { session: false }), update);
router.delete('/', passport.authenticate('jwt', { session: false }), remove);

export default router;
