import mongoose from 'mongoose';
import jwtDecode from 'jwt-decode';

const Users = mongoose.model('Users');

export default async (req, res) => {
  try {
    const token = req.headers.authorization.replace('Bearer ', '');
    const activeUser = jwtDecode(token);
    const { isAdmin } = activeUser;

    if (!isAdmin) {
      res.status(403);
      return res.send('Access denied');
    }

    const users = await Users.find({});

    return res.send(users.map(user => ({
      id: user.id,
      name: user.name,
      email: user.email,
      isAdmin: user.isAdmin,
    })));
  } catch (e) {
    res.status(500);
    res.send('Something going wrong');
  }
};
