import mockingoose from 'mockingoose';

import '../../../models/user';
import '../../../models/request';

import userList from '../users-list';

const fakeAdminToken = 'Bearer eyJhbGciOiJIUzI1NiJ9.eyJpc0FkbWluIjp0cnVlLCJfaWQiOiIxIn0.0hlJx77CWgtltAVkiLHSd6Gq5zjFI85PEz1NIFo9cqw';
const fakeNonAdminToken = 'Bearer eyJhbGciOiJIUzI1NiJ9.eyJpc0FkbWluIjpmYWxzZSwiX2lkIjoiMSJ9.ZrrnPCGODbArFI16ArtZR9tUoBHtbScpRDpZkA9ve_k';

const _doc = {
  _id: '5b0ffc10862079144aa2817d',
  name: 'name',
  email: 'name@email.com',
  isAdmin: false,
};

mockingoose.Users.toReturn([_doc]);

describe('get users list should works as designed', () => {
  it('Admin can get users list', async () => {
    const req = {
      headers: {
        authorization: fakeAdminToken,
      },
    };

    const res = {
      status: jest.fn(),
      send: jest.fn(),
    };

    await userList(req, res);

    const expectedResult = [{
      id: '5b0ffc10862079144aa2817d',
      name: 'name',
      email: 'name@email.com',
      isAdmin: false,
    }];

    expect(res.send.mock.calls.length)
      .toBe(1);
    expect(res.send.mock.calls[0][0])
      .toEqual(expectedResult);
  });

  it('Non Admin can\'t get users list', async () => {
    const req = {
      headers: {
        authorization: fakeNonAdminToken,
      },
    };

    const res = {
      status: jest.fn(),
      send: jest.fn(),
    };

    await userList(req, res);

    expect(res.send.mock.calls.length)
      .toBe(1);
    expect(res.send.mock.calls[0][0])
      .toBe('Access denied');
    expect(res.status.mock.calls.length)
      .toBe(1);
    expect(res.status.mock.calls[0][0])
      .toBe(403);
  });
});
