import mockingoose from 'mockingoose';

import '../../../models/user';
import '../../../models/request';

import remove from '../remove';

const fakeAdminToken = 'Bearer eyJhbGciOiJIUzI1NiJ9.eyJpc0FkbWluIjp0cnVlLCJfaWQiOiIxIn0.0hlJx77CWgtltAVkiLHSd6Gq5zjFI85PEz1NIFo9cqw';
const fakeNonAdminToken = 'Bearer eyJhbGciOiJIUzI1NiJ9.eyJpc0FkbWluIjpmYWxzZSwiX2lkIjoiMSJ9.ZrrnPCGODbArFI16ArtZR9tUoBHtbScpRDpZkA9ve_k';

const _doc = {
  _id: '2',
  name: 'name',
  email: 'name@email.com',
};

mockingoose.Users.toReturn(_doc, 'findOne');

describe('remove User should works as designed', () => {
  it('Admin can remove user', async () => {
    const removalId = 2;
    const req = {
      headers: {
        authorization: fakeAdminToken,
      },
      query: {
        id: removalId,
      },
    };

    const res = {
      status: jest.fn(),
      send: jest.fn(),
    };

    await remove(req, res);

    expect(res.send.mock.calls.length)
      .toBe(1);
    expect(res.send.mock.calls[0][0])
      .toBe(`user ${removalId} deleted`);
  });

  it('Non Admin can\'t remove user and should get 403', async () => {
    const removalId = 2;
    const req = {
      headers: {
        authorization: fakeNonAdminToken,
      },
      query: {
        id: removalId,
      },
    };

    const res = {
      status: jest.fn(),
      send: jest.fn(),
    };

    await remove(req, res);

    expect(res.send.mock.calls.length)
      .toBe(1);
    expect(res.send.mock.calls[0][0])
      .toBe('Access denied');
    expect(res.status.mock.calls.length)
      .toBe(1);
    expect(res.status.mock.calls[0][0])
      .toBe(403);
  });
  it('Id is required, server should response with 400 if id is not passed', async () => {
    const req = {
      headers: {
        authorization: fakeAdminToken,
      },
      query: {},
    };

    const res = {
      status: jest.fn(),
      send: jest.fn(),
    };

    await remove(req, res);

    expect(res.send.mock.calls.length)
      .toBe(1);
    expect(res.send.mock.calls[0][0])
      .toBe('Id is required');
    expect(res.status.mock.calls.length)
      .toBe(1);
    expect(res.status.mock.calls[0][0])
      .toBe(400);
  });
  it('Server should response with 404 if user is not exist', async () => {
    mockingoose.resetAll();

    const req = {
      headers: {
        authorization: fakeAdminToken,
      },
      query: {
        id: 'not found id',
      },
    };

    const res = {
      status: jest.fn(),
      send: jest.fn(),
    };

    await remove(req, res);

    expect(res.send.mock.calls.length)
      .toBe(1);
    expect(res.send.mock.calls[0][0])
      .toBe('Not found');
    expect(res.status.mock.calls.length)
      .toBe(1);
    expect(res.status.mock.calls[0][0])
      .toBe(404);
  });
});
