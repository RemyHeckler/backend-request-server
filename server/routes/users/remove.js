import mongoose from 'mongoose';
import jwtDecode from 'jwt-decode';


const Users = mongoose.model('Users');
const Request = mongoose.model('Request');

export default async (req, res) => {
  try {
    const token = req.headers.authorization.replace('Bearer ', '');
    const activeUser = jwtDecode(token);
    const { _id, isAdmin } = activeUser;
    const { id } = req.query;

    if (!id) {
      res.status(400);
      return res.send('Id is required');
    }

    if (!isAdmin && _id !== id) {
      res.status(403);
      return res.send('Access denied');
    }

    const user = await Users.findOne({ _id: req.query.id });

    if (!user) {
      res.status(404);
      return res.send('Not found');
    }

    await Request.deleteMany({ author: id });
    await Users.findByIdAndRemove(id);
    return res.send(`user ${id} deleted`);
  } catch (e) {
    res.status(500);
    res.send('Something going wrong');
  }
};
