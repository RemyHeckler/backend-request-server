import mongoose from 'mongoose';
import jwtDecode from 'jwt-decode';

const Users = mongoose.model('Users');

export default async (req, res) => {
  try {
    const token = req.headers.authorization.replace('Bearer ', '');
    const activeUser = jwtDecode(token);
    const { _id, isAdmin } = activeUser;
    const id = req.query.id || _id;

    if (id !== _id && !isAdmin) {
      res.status(403);
      res.send('Access denied');
    }

    const user = await Users.findOne({ _id: id });

    if (!user) {
      res.status(404);
      return res.send('Not found');
    }

    return res.send({ _id: user._id, name: user.name, email: user.email });
  } catch (e) {
    res.status(500);
    res.send('Something going wrong');
  }
};
