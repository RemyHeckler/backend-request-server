import mongoose from 'mongoose';
import jwtDecode from 'jwt-decode';
import bcrypt from 'bcryptjs';

const Users = mongoose.model('Users');

export default async (req, res) => {
  try {
    const token = req.headers.authorization.replace('Bearer ', '');
    const activeUser = jwtDecode(token);
    const { _id, isAdmin } = activeUser;
    const { name, email, password } = req.body;
    const salt = bcrypt.genSaltSync(10);
    const id = req.query.id || req.body.id || _id;

    if (email) {
      req.checkBody('email', 'Please enter a valid email').isEmail();
    }

    if (password) {
      req.checkBody('confirmPassword', 'Confirm Password Must Matches With Password')
        .equals(req.body.password);
    }

    const errors = req.validationErrors();

    if (errors) {
      res.status(400);
      return res.send({ errors });
    }

    if (id !== _id && !isAdmin) {
      res.status(403);
      res.send('Access denied');
    }

    const user = await Users.findOne({ _id: id });

    if (!user) {
      res.status(404);
      return res.send('Not found');
    }

    const values = JSON.parse(JSON.stringify({
      name,
      email,
      password: password ? bcrypt.hashSync(req.body.password, salt) : undefined,
    }));

    const updatedUser = await Users
      .findOneAndUpdate({ _id: user._id }, { ...values }, { new: true }).exec();
    return res.send({
      name: updatedUser.name, email: updatedUser.email, _id, isAdmin: updatedUser.isAdmin,
    });
  } catch (e) {
    console.log(e);
    res.status(500);
    res.send('Something going wrong');
  }
};
