import mongoose from 'mongoose';

const { Schema } = mongoose;

const UsersSchema = new Schema({
  name: { type: String },
  email: { type: String },
  password: { type: String },
  isAdmin: { type: Boolean },
});

export default mongoose.model('Users', UsersSchema);
