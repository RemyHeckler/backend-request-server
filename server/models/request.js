import mongoose from 'mongoose';

const { Schema } = mongoose;

const RequestSchema = new Schema({
  title: { type: String },
  body: { type: String },
  date: { type: Date },
  completed: { type: Boolean },
  author: {
    type: Schema.Types.ObjectId,
    ref: 'Users',
  },
});

export default mongoose.model('Request', RequestSchema);
